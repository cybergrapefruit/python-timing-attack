# Python timing attack

This program is a demonstration of a Python timing attack. It utilises this timing attack to guess a randomly generated password of 5 lowercase ASCII characters.

## Disclaimer

The code and program provided here is intended to be used for educational and research purposes ONLY. DO NOT attempt to use any of the code here or the program for illegal purposes.

## Licensing

Some code in this repo was adapted from code by not1cyyy. The specific section of code that was inspired can be found below:
```
# establishes a base line
base_times = timeit.repeat(
    "s1 == s2", f"s1, s2 = {guessed!r}, {password!r}", repeat=10)
# gets the time of the current letter
guess_times = timeit.repeat("s1 == s2", f"s1, s2 = {
    guessed[:current_index] + letter + guessed[current_index+1:]!r}, {password!r}", repeat=10)
# checks if the guessed letter is likely to be correct
if min(guess_times) > min(base_times):
```

This is from a [repository](https://github.com/not1cyyy/Timing-Attack) owned by not1cyyy and is licensed under GPL-3.0 (not endorsed by not1cyyy). The code written is therefore also licensed under GPL-3.0.