"""A module used to demonstrate a python timing attack

Copyright (C) 2024 Cybergrapefruit

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>"""

import timeit
from string import ascii_lowercase
import random


password = "".join(random.choices(ascii_lowercase, k=5))
repeat_count = 10


def brute_force(guessed, current_index):
    """Tries to brute force each lowercase ASCII letter in the current position"""
    loop = 0
    confidencies = {}  # stores the confidence level of each letter
    # loops until there is a single letter left or until the loop exceeds 30 (which it should not in normal circumstances)
    while len(confidencies) != 1 and loop < 30:
        # immediately ends if there are more than 10 items left on loop 10 (this should not happen in normal circumstances)
        if len(confidencies) >= 10 and loop >= 10:
            return [None, None, None]  # a dummy list
        for letter in ascii_lowercase:
            # skips letters which have lost confidence
            if letter not in confidencies and loop > 1:
                continue
            # establishes a base time
            base_times = timeit.repeat(
                "s1 == s2", f"s1, s2 = {guessed!r}, {password!r}", repeat=repeat_count)
            # gets the guess time of the current letter
            guess_times = timeit.repeat("s1 == s2", f"s1, s2 = {
                guessed[:current_index] + letter + guessed[current_index+1:]!r}, {password!r}", repeat=repeat_count)
            # checks if the guessed letter is likely to be correct
            if min(guess_times) > min(base_times):
                # if it is likely to be correct, adjust confidences accordingly
                if letter in confidencies:
                    confidencies[letter] += 1
                elif letter not in confidencies and loop < 2:
                    confidencies[letter] = 1
            # removes any letters which have lost confidence
            if letter in confidencies:
                if confidencies[letter] < loop:
                    del confidencies[letter]
        # outputs the progress of the current guess 
        if loop >= 2:
            print(f"There are {len(confidencies.keys(
            ))} possibilities left for the letter in position {current_index+1}")
        loop += 1
    return list(confidencies.keys())


def start_attack():
    """Starts the timing attack"""
    guessed = "-"*len(password)
    current_position = 0
    repeated_try = False
    # loops until all letters but the last are found
    while guessed[-2] == "-":
        print(guessed)
        # tries to get the letter for the current position
        results = brute_force(guessed, current_position)
        # if a strange output is recieved then retry the same position
        if (len(results) > 1 or len(results) == 0) and not repeated_try:
            print("Something is wrong with the current brute force attempt, trying again")
            repeated_try = True
            continue
        # if the same position has already been tried before, the check the previous letter again
        if repeated_try:
            print(
                "Something probably wrong with the previous letter, trying previous letter again")
            if current_position == 0:
                continue
            current_position = (current_position - 1) % len(password)
            guessed = guessed[:current_position] + \
                "-" + guessed[current_position+1:]
            repeated_try = False
            continue
        repeated_try = False
        guessed = guessed[:current_position] + \
            results[0] + guessed[current_position+1:]
        current_position += 1
        print(f"The letter at position {current_position} is {results[0]}")
    print(f"The first {len(password)-1} letters of the password is {guessed[:-1]}")
    print("Now brute forcing the last letter...")
    for letter in ascii_lowercase:
        if guessed[:-1] + letter == password:
            print(f"The password is {guessed[:-1] + letter}")
            exit()
    print("The attack has failed please run it again...")

if __name__ == "__main__":
    start_attack()
